#I have a four column database that uses the timestamp as the primary key and the other three are called:
# AdsBlocked, QuerysToday, and TrafficPercentage

import urllib.request
import json
import pymysql.cursors

mydb = pymysql.connect(user='<USER>', password='<PASS>', host='<IP HERE>',database='<DATABASE HERE')
cursor = mydb.cursor()

#Can call function to just list tables. useful for testing permissions for SQL user and without inserting data
def test_db_conn():
	sql = "SHOW TABLES"
	cursor.execute(sql)
	result = cursor.fetchone()
	print(result)


def get_pihole_data():
	with urllib.request.urlopen('pi-hole api url path goes here') as page:
		return(page.read().decode('utf-8'))

#Probably a better way of doing this but this mostly uses core python libraries
def parse_pihole_data(json_input):
	parsed_json = json.loads(json_input)
	domainsblocked = parsed_json['domains_being_blocked']
	dnstoday = parsed_json['dns_queries_today']
	adstoday = parsed_json['ads_blocked_today']
	adspercentage = parsed_json['ads_percentage_today']
	return domainsblocked, dnstoday, adstoday, adspercentage

#inserts the fields
def insert_data(ads, dns, percentage):
	sql = "INSERT INTO `<TABLE>` (<Adscolumn>, <DNSColumn>, <Traffic_Percent>) VALUES (%s, %s, %s); "
	cursor.execute(sql, (ads, dns, percentage))
	mydb.commit()
	mydb.close()

#Verify last insert data below

def verify_data():
	pass

def main():
	json_data = get_pihole_data()
	blocked, dns, ads, percentage = parse_pihole_data(json_data)
	insert_data(ads, dns, percentage)
	print(data inserted sucessfully
main()
